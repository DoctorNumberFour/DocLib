# **DocLib**

A chatbot library for euphoria.io.

# Usage

## Installation

To install, use `pip` or `pip3`:

```bash
pip3 install doclib
```

**WINDOWS USERS: YOU MUST [INSTALL WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10 "A tutorial for doing so") AND RUN THIS ON IT IN ORDER FOR IT TO WORK**

When beginning a new bot, import the `Bot` class:

```python
from doclib import Bot
```

## Basic Bots

A basic bot will be a `Bot` object, constructed with a `nick`(the bot's name, shown in the room), a `room`(where the bot is sent by default, defaults to bots or test in debug mode), and an `owner`(your euphoria username.):

```python
sampleBot = Bot(nick = 'bugBot', room = 'test', owner = 'sample user')
```

### Constructor Options

| option         | default                                                                                                                                                                                                            | usage                                                                                                                                        |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | :------------------------------------------------------------------------------------------------------------------------------------------- |
| `nick`         | none, this is required                                                                                                                                                                                             | The nick the bot will enter the room with.                                                                                                   |
| `room`         | `"bots"`                                                                                                                                                                                                           | The default room for the bot to be sent to.                                                                                                  |
| `owner`        | `"nobody"`                                                                                                                                                                                                         | The owner/creator of the bot (you).                                                                                                          |
| `password`     | blank string                                                                                                                                                                                                       | room password (used for private rooms)                                                                                                       |
| `help`         | `"[nick] is a bot made with Doctor Number Four's Python 3 bot library, DocLib (link: https://gitlab.com/DoctorNumberFour/DocLib) by @[owner]. `<br>`It follows botrulez and does not have a custom !help message yet."` | your custom !help command response. can also be set by adding a `"^!help @[nick w/o spaces]$"` command to your regexes                       |
| `ping`         | `"Pong!"`                                                                                                                                                                                                          | your custom !ping response.                                                                                                                  |
| `important`    | `False`                                                                                                                                                                                                            | if `True`, the bot will only be killable by a roomhost or the owner. **NOT RECOMMENDED FOR SIMPLE BOTS OR BOTS PRONE TO SPAM**.              |
| `killed`       | `"/me has died"`                                                                                                                                                                                                   | your custom message sent when bot is !killed.                                                                                                |
| `API_timeout`  | `10`                                                                                                                                                                                                               | ADVANCED: number of API responses to check for command responses. You should not need to change this unless the room is particularly active. |
| `advanced`     | `False`                                                                                                                                                                                                            | ADVANCED: if `True`, declares the bot as advanced, enabling the user to directly handle any API response (while still obeying botrulez)      |
| `function`     | `None`                                                                                                                                                                                                             | ADVANCED: specifies the universal advanced start handler function.                                                                           |
| `customArgs`   | `False`                                                                                                                                                                                                            | ADVANCED: Removes the set of default command-line arguments for bots.                                                                        |
| `sendable`     | `False`                                                                                                                                                                                                            | If `True`, enables the bot to be sent to other rooms with the !send @bot &room command.                                                      |
| `human`        | `False`                                                                                                                                                                                                            | If `True`, the bot shows up in the People list. If used improperly, this is against Euphoria's ToS.                                          |
| `killChildren` | `False`                                                                                                                                                                                                            | If `True`, when killed, the bot will kill any copies it has made as well.                                                                    |

Once constructed, the user should assign a regex dictionary for non-advanced use cases:

```python
sampleBot.set_regexes({r'^!command$': function,
  r'(?i)word': 'response',
  r'^!bunchamessages$':
    {
    "message": "nick to use",
    "second message": "different nick"
    },
  r'^!afewmessages$':
    [
    "first message",
    "second message"
    ]
  }
)
```

#### Regex Formatting

DocLib uses the default python `re` library for regex formatting.

## Custom Function Responses

Regexes with a function value run the function with a `Bot` object (`chatbot`), an `AttrDict` (`msg`), and the regex `match` object (`matches`). Functions can utilise any of the numerous `Bot` methods and all return the API's reply as an `AttrDict`:

| Method         | Parameters                                                           | Effect                                                                                                                                                                                                                                                                        |
| -------------- | -------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `send_msg`     | `parent` (`AttrDict` or string), `msgString` (string)                | Sends `msgString` as a reply to `parent`, where `parent` can either be a string containing the message id returned by the API or an `AttrDict` with the structure of a `send-reply` generated by the API (recommended: use the `msg` parameter given to your custom function) |
| `set_nick`     | `nick` (string)                                                      | Sets the bot's name to `nick`                                                                                                                                                                                                                                                 |
| `get_userlist` | none                                                                 | returns an array of user `AttrDict`s currently in the room, including bots, accounts, and agents.                                                                                                                                                                             |
| `move_to`      | `roomName` (string), `password` (string, default is an empty string) | Moves the bot to a different room, specified by `roomName`, and, if necessary, attempts to log into it using `password`.                                                                                                                                                      |
| `restart`      | none                                                                 | Restarts the bot.                                                                                                                                                                                                                                                             |
| `kill`         | none                                                                 | Kills the bot.                                                                                                                                                                                                                                                                |
| `copy_to`      | `roomName` (string), `password` (string, default is an empty string) | Creates a new `Bot` object set to appear in &`roomName` and returns it. The user can then start it in a separate thread with the `start_in_new_thread` function.                                                                                                              |
| `initiate_pm`  | `id` (string), `bot` (`Bot`, default None)                           | Starts a PM with the user `id`, placing `bot` in said PM room if specified, else a copy of the bot receiving the command.                                                                                                                                                     |
| `login`        | `email` (string), `password` (string)                                | Logs bot in as a user. Takes effect on reconnect, and the bot will show up in the People list.                                                                                                                                                                                |

To log errors and info, use `Bot.log` with a `message` string and the optional `error` boolean. (note: NOT a `Bot` subfunction) To choose between a set of responses to a command randomly (including dictionaries, strings, and even functions), use `Bot.randomize_between(*responses)`.

### Example:

Regex:

```python
sampleBot.set_regexes({'^!alive$': alive})
```

Function (defined **above bot start**)

```python
def alive(chatbot, msg, matches):
    chatbot.send_msg(parent = msg, msgString = '/me IS ALIVE!')
    chatbot.set_nick('Thunder')
    chatbot.send_msg(parent = msg, msgString = '/me crashes')
    chatbot.set_nick('sampleBot')
```

## Custom Handlers

To set custom function handlers for any API response (when not `advanced`, this does not include `ping-event`s, `send-event`s, `error`s, or `bounce-event`s), use `set_handler`:

```python
sampleBot.set_handler(eventString = "join-event", on_join)
```

or

```python
sampleBot.set_handlers({"joinEvent": on_join})
```

All handler functions, including the universal function parameter for advanced start, are passed the API response (`msg`) as an `AttrDict` every time the corresponding event is received. Custom handlers take precedence over advanced start's universal handler.

# Startup

Once the bot is set up and populated with commands, the user can connect and start it:

```python
sampleBot.connect()
sampleBot.start()
```

To start a bot in a new thread (useful for copies), use the `start_in_new_thread` function instead:

```python
copy.connect()
copy.start_in_new_thread()
```

## Advanced Usage

Using an `advanced` value of `True` (whether in the bot's declaration or its start function) enables the user to set a universal handler for all API events, in addition to any custom handlers:

```python
sampleBot.start(advanced = True, function = handler_function)
```

If unspecified, advanced start uses a filler function that does nothing, enabling the user to use entirely custom handlers for every API response.

# Command Line Options

When running any bot in the command line, the user has access to a few default options:

| option                               | usage                                                                     |
| ------------------------------------ | :------------------------------------------------------------------------ |
| `-t`, `--test`, `--debug`            | Used to debug dev builds. Sends bot to &test instead of its default room. |
| `--room ROOM`, `-r ROOM`             | Sends bot to &ROOM instead of its default room.                           |
| `--password PASSWORD`, `-p PASSWORD` | Password to use should the room be locked.                                |

To add new arguments, use the bot's `add_command_line_arg` function as you would the default Python `argparse.add_argument` function. It returns the value of the added argument if provided a `name` parameter. To remove the default arguments, set `customArgs` in the `Bot` constructor to `True`.
